package logica;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import bean.UsuarioBean;
import dao.UsuarioDao;

@ManagedBean(name="login")
public class Login {
	@ManagedProperty(value="#{usuarioBean}")
	UsuarioBean usuario;
	
	public boolean validar(){
		if(usuario==null)
			System.out.println("Error----------------");
		else
			System.out.println(usuario.getNome()+"++++++++++++++++++++++++++++");
		UsuarioDao usuarioDao = new UsuarioDao();
		if (usuarioDao.validarUsuario(usuario.getNome(), usuario.getSenha()))
			return true;
		else
			return false;
		
	}

	public UsuarioBean getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}
	
	
}
