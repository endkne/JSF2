package logica;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import bean.UsuarioBean;
import dao.UsuarioDao;

@ManagedBean(name="salvarUsuario",eager=true)
@SessionScoped
public class SalvarUsuario {
	public UsuarioBean getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	@ManagedProperty(value="#{usuarioBean}")
	UsuarioBean usuario;
	
	public String salvar(){
		if(usuario == null){
			System.out.println("Deu algum problema");
		}else{
			UsuarioDao usuarioDao = new UsuarioDao();
			usuarioDao.salvarUsuario(usuario.getNome(), usuario.getIdade(), usuario.getSenha());
		}
		return "index.xhtml";
	}
	
}
