package resources;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import bean.UsuarioBean;

@ManagedBean(name="helloWord", eager=true)
@SessionScoped
public class HelloWorld {
	@ManagedProperty(value = "#{loginBean}")
	private UsuarioBean user;
	private String message;
	private String message2;
	
	public HelloWorld() {
		System.out.println("Classe helloWord criada");
	}
	
	public String getMessage(){
		if (this.user != null){
			this.message = this.user.getNome();
		}
		return this.message;
	}
	
	public String getMessage2() {
		if (this.user != null){
			this.message2 = this.user.getNome() + this.user.getSenha();
		}
		return this.message2;
	}

	public UsuarioBean getUser() {
		return user;
	}

	public void setUser(UsuarioBean user) {
		this.user = user;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMessage2(String message2) {
		this.message2 = message2;
	}
	
}
